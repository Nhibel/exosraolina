package com.aston.raolina.domaine;

public class GrandeVitesseException extends VitesseException{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public GrandeVitesseException(double vitesse, Vehicule vehicule) {
		super(vitesse, vehicule);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		String Erreur = "Vitesse trop haute !";
		return Erreur + super.toString();
	}

}
