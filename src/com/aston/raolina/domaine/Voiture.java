package com.aston.raolina.domaine;

public class Voiture extends Vehicule {

	private double vitesseMax;

	private static int nbreVoiture = 0;

	public Voiture() {
		this("Renault", 50);
	}

	public Voiture(String marque, int puissanceMoteur) {
		this(marque, puissanceMoteur, 100, 120);
	}

	public Voiture(String marque, int puissanceMoteur, double poidsMoteur, double poidsChassis) {
		super(marque, puissanceMoteur, poidsMoteur, poidsChassis);
		this.calculerPoidsTotal();
		this.calculerVitesseMax();
		nbreVoiture++;
	}

	@Override
	public void afficher() {
		super.afficher();
		System.out.println("Nombre de voitures instanciées : " + nbreVoiture);
		System.out.println("Vitesse max : " + this.vitesseMax);

	}

	private double calculerVitesseMax() {
		this.vitesseMax = super.getPoidsTotal() * this.moteur.getPuissanceMoteur() / 500;
		return this.vitesseMax;
	}

	@Override
	public void rouler() throws VitesseException {
		try {
			if (this.vitesseMax < 50) {
				throw new BasseVitesseException(this.vitesseMax, this);
			} else if (this.vitesseMax > 120) {
				throw new GrandeVitesseException(this.vitesseMax, this);
			} else {
				System.out.println(
						"ça roule pour la voiture de marque " + this.marque + " a la vitesse de : " + this.vitesseMax);
			} } catch (VitesseException e) {
				System.out.println(e.toString());
				e.printStackTrace();
			}
	}



	@Override
	public void finalize() {
		nbreVoiture--;
	}
}
