package com.aston.raolina.domaine;

public class Moteur {
	private int puissanceMoteur = 55;
	private double poidsMoteur = 100;

	public Moteur() {
		this(55, 100);
	}

	public Moteur(int puissanceMoteur, double poidsMoteur) {
		this.puissanceMoteur = puissanceMoteur;
		this.poidsMoteur = poidsMoteur;
	}

	public void afficher() {
		System.out.println("Puissance Moteur : " + this.puissanceMoteur + " Poids Moteur : " + this.poidsMoteur);
	}

	public int getPuissanceMoteur() {
		return this.puissanceMoteur;
	}
	public void setPuissanceMoteur(int puissanceMoteur) {
		this.puissanceMoteur = puissanceMoteur;
	}
	public double getPoidsMoteur() {
		return this.poidsMoteur;
	}
	public void setPoidsMoteur(double poidsMoteur) {
		this.poidsMoteur = poidsMoteur;
	}

}
