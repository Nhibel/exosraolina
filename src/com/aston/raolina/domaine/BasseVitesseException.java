package com.aston.raolina.domaine;

public class BasseVitesseException extends VitesseException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public BasseVitesseException(double vitesse, Vehicule vehicule) {
		super(vitesse, vehicule);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		String Erreur = "Vitesse trop basse !";
		return Erreur + super.toString();
	}




}
