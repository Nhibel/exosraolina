package com.aston.raolina.domaine;

public class Chassis {
	private double poidsChassis = 120;

	public Chassis() {
	}

	public Chassis(double poidsChassis) {
		this.poidsChassis = poidsChassis;
	}

	public void afficher() {
		System.out.println("Poids chassis : " + this.poidsChassis);
	}

	public double getPoidsChassis() {
		return this.poidsChassis;
	}

	public void setPoidsChassis(double poidsChassis) {
		this.poidsChassis = poidsChassis;
	}

}
