package com.aston.raolina.domaine;

public class Camion extends Vehicule {
	private static int nbreCamion = 0;
	private double vitesseMax;

	public Camion() {
		this("Iveco", 50);
	}

	public Camion(String marque, int puissanceMoteur) {
		this(marque, puissanceMoteur, 100, 120);
	}

	public Camion(String marque, int puissanceMoteur, double poidsMoteur, double poidsChassis) {
		super(marque, puissanceMoteur, poidsMoteur, poidsChassis);
		nbreCamion++;
	}

	private double calculerVitesseMax() {
		this.vitesseMax = super.getPoidsTotal() * this.moteur.getPuissanceMoteur() / 700;
		return this.vitesseMax;
	}

	@Override
	public void rouler() {
		System.out.println("ça roule pour le camion de marque "+this.marque);
	}

	@Override
	public void afficher() {
		super.afficher();
		System.out.println("Vitesse max : "+this.calculerVitesseMax());
		System.out.println("Nombre de camion instanciés : "+nbreCamion);
	}

	@Override
	public void finalize() {
		nbreCamion--;
		System.out.println("Destruction système");
	}
}
