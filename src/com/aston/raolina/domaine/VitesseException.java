package com.aston.raolina.domaine;

public class VitesseException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	protected double vitesse;
	protected Vehicule vehicule;

	public VitesseException(double vitesse, Vehicule vehicule) {
		this.vitesse = vitesse;
		this.vehicule = vehicule;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VitesseException [vitesse=");
		builder.append(this.vitesse);
		builder.append(", vehicule=");
		builder.append(this.vehicule.getMarque());
		builder.append("]");
		return builder.toString();
	}
}
