package com.aston.raolina.domaine;

abstract public class Vehicule {
	protected String marque;

	private double vitesseMax;
	private double poidsTotal;

	protected Moteur moteur;
	protected Chassis chassis;

	public Vehicule() {
		this("Renault", 50);
	}

	public Vehicule(String marque, int puissanceMoteur) {
		this(marque, puissanceMoteur, 100, 120);
	}

	public Vehicule(String marque, int puissanceMoteur, double poidsMoteur, double poidsChassis) {
		this.moteur = new Moteur();
		this.chassis = new Chassis();
		this.marque = marque;
		this.moteur.setPuissanceMoteur(puissanceMoteur);
		this.moteur.setPoidsMoteur(poidsMoteur);
		this.chassis.setPoidsChassis(poidsChassis);
	}

	protected double calculerPoidsTotal() {
		this.poidsTotal = this.moteur.getPoidsMoteur() + this.chassis.getPoidsChassis();
		return this.poidsTotal;
	}

	public void afficher() {
		System.out.println("Marque : "+this.marque+" Poids Total : "+ this.getPoidsTotal());
		this.moteur.afficher();
		this.chassis.afficher();
	}

	public String getMarque() {
		return this.marque;
	}

	public double getVitesseMax() {
		return this.vitesseMax;
	}

	public double getPoidsTotal() {
		return this.poidsTotal;
	}

	public abstract void rouler() throws VitesseException;

}
