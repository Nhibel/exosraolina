package com.aston.raolina.tps;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Tp01 {

	public static void main(String[] args) {

		Hashtable<Integer, String> monHashtable = new Hashtable<Integer, String>();
		monHashtable.put(new Integer(1), "Janvier");
		monHashtable.put(new Integer(2), "Février");
		monHashtable.put(new Integer(3), "Mars");
		monHashtable.put(new Integer(4), "Avril");
		monHashtable.put(new Integer(5), "Mai");
		monHashtable.put(new Integer(6), "Juin");
		monHashtable.put(new Integer(7), "Juillet");
		monHashtable.put(new Integer(8), "Aout");
		monHashtable.put(new Integer(9), "Septembre");
		monHashtable.put(new Integer(10), "Octobre");
		monHashtable.put(new Integer(11), "Novembre");
		monHashtable.put(new Integer(12), "Décembre");

		// Parcour la map
		// Iterator emplace avantageusement Enumeration
		// Permet par exemple de supprimer les éléments de la collection au fur et à
		// mesure.

		System.out.println("Mon Hash Table : ");

		for (Integer key : monHashtable.keySet()) {
			System.out.println("key = " + key + " value = " + monHashtable.get(key));
		}
		System.out.println("");

		//////////////////////////////////////////////////////////////////////////////////////

		TreeMap<Integer, String> monTreeMap = new TreeMap<Integer, String>();
		monTreeMap.put(new Integer(1), "Janvier");
		monTreeMap.put(new Integer(2), "Février");
		monTreeMap.put(new Integer(3), "Mars");
		monTreeMap.put(new Integer(4), "Avril");
		monTreeMap.put(new Integer(5), "Mai");
		monTreeMap.put(new Integer(6), "Juin");
		monTreeMap.put(new Integer(7), "Juillet");
		monTreeMap.put(new Integer(8), "Aout");
		monTreeMap.put(new Integer(9), "Septembre");
		monTreeMap.put(new Integer(10), "Octobre");
		monTreeMap.put(new Integer(11), "Novembre");
		monTreeMap.put(new Integer(12), "Décembre");

		System.out.println("Mon Tree Map : ");
		for (Integer key : monTreeMap.keySet()) {
			System.out.println("key = " + key + " value = " + monTreeMap.get(key));
		}
		System.out.println("");

		//////////////////////////////////////////////////////////////////////////////////////

		Map<Integer, String> monHashMap = new HashMap<Integer, String>();
		monHashMap.put(new Integer(1), "Janvier");
		monHashMap.put(new Integer(2), "Février");
		monHashMap.put(new Integer(3), "Mars");
		monHashMap.put(new Integer(4), "Avril");
		monHashMap.put(new Integer(5), "Mai");
		monHashMap.put(new Integer(6), "Juin");
		monHashMap.put(new Integer(7), "Juillet");
		monHashMap.put(new Integer(8), "Aout");
		monHashMap.put(new Integer(9), "Septembre");
		monHashMap.put(new Integer(10), "Octobre");
		monHashMap.put(new Integer(11), "Novembre");
		monHashMap.put(new Integer(12), "Décembre");

		// Parcour la map
		System.out.println("Mon HashMap : ");

		for (Entry<Integer, String> entry : monHashMap.entrySet()) {
			Integer cle = entry.getKey();
			String valeur = entry.getValue();

			System.out.println(valeur);
		}
		System.out.println("");
	}

}
