package com.aston.raolina.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.aston.raolina.domaine.Camion;
import com.aston.raolina.domaine.Vehicule;
import com.aston.raolina.domaine.VitesseException;
import com.aston.raolina.domaine.Voiture;

//Dans la méthode main() de CMain:
//Créer un tableau listeVehicule de 5 références sur CVehicule :
//le premier élément du tableau doit référencer une instance de CVoiture("Peugeot", 100)
//le deuxième élément du tableau doit référencer une instance de CCamion("Iveco", 200)
//Ecrire une boucle qui parcourt le tableau pour appeler la méthode rouler() des différentes instances.
//Corriger l'erreur de compilation : Method rouler() not found in CVehicule.
//Relancer le programme.

public class Main {

	public static void main(String[] args) throws VitesseException {
		// TODO Auto-generated method stub

		Voiture voiture = new Voiture();
		voiture.afficher();
		voiture.rouler();
		System.out.println("");

		Voiture voiture2 = new Voiture("Honda", 50);
		voiture2.afficher();
		voiture2.rouler();
		System.out.println("");

		Voiture voiture3 = new Voiture("Chrysler", 90, 130, 100);
		voiture3.afficher();
		voiture3.rouler();
		System.out.println("");

		Camion camion = new Camion();
		camion.afficher();
		camion.rouler();
		System.out.println("");

		Camion camion2 = new Camion("Ford", 130);
		camion2.afficher();
		camion2.rouler();
		System.out.println("");

		camion = null;
		System.gc();

		camion2.afficher();

		Vehicule listeVehicules[] = new Vehicule[5];

		listeVehicules[0] = new Voiture("Peugeot", 100);
		listeVehicules[1] = new Camion("Iveco", 200);

		for (Vehicule vehicule : listeVehicules) {
			if (vehicule instanceof Voiture && vehicule != null) {
				((Voiture) vehicule).rouler();
			} else if (vehicule instanceof Camion && vehicule != null) {
				((Camion) vehicule).rouler();
			}
		}

		System.out.println("");
		System.out.println("Je suis dans le ArrayList : ");

		ArrayList<Vehicule> vehiculeArray = new ArrayList<Vehicule>();
		vehiculeArray.add(voiture);
		vehiculeArray.add(camion2);

		for (int i = 0; i <= vehiculeArray.size() - 1; i++) {
			if (vehiculeArray.get(i) instanceof Voiture) {
				((Voiture) vehiculeArray.get(i)).rouler();
			} else if (vehiculeArray.get(i) instanceof Camion) {
				((Camion) vehiculeArray.get(i)).rouler();
			}
		}
		System.out.println("");

		System.out.println("Je suis dans le listiterator");

		Iterator<Vehicule> iterator = vehiculeArray.iterator();

		Vehicule v;

		while (iterator.hasNext()) {
			v = iterator.next();
			v.rouler();
		}

		Camion camion3 = new Camion();

		Map<Integer, Vehicule> hVehicule = new HashMap<Integer, Vehicule>();

		hVehicule.put(new Integer(1), voiture);
		hVehicule.put(new Integer(2), voiture2);
		hVehicule.put(new Integer(3), camion2);
		hVehicule.put(new Integer(4), camion3);

		for (Entry<Integer, Vehicule> entry : hVehicule.entrySet()) {
			//Integer cle = entry.getKey();
			Vehicule valeur = entry.getValue();

			if (valeur.getMarque() == "Iveco") {
				System.out.println("Je suis un camion Iveco !");
				valeur.afficher();
			}
		}

	}
}
